#!/bin/sh

###########
# Variables
###########

#Required Variables
#BashRC File
FILE_BASH="$HOME/.bashrc";
FILE_WPAB="$HOME/wpautobuild.sh";

#The parent folder for all your site files
PATH_DEV="C:/wamp3/www/dev/sites";

#Virtual Host path
FILE_VIRTUALHOST="C:/wamp3/bin/apache/apache2.4.33/conf/extra/httpd-vhosts.conf";

#System host path
FILE_HOST="C:/Windows/System32/drivers/etc/hosts";

#MYSQL Variables
MYSQL_UN="root";
MYSQL_PW="";

#Repositories
declare -A REPO_PATH;
REPO_PATH[jumpstart]="https://chomachomachoma@bitbucket.org/chomachomachoma/wp-jumpstart.git";
REPO_PATH[divi]="";

declare -A WP_DB_PREFIX;
WP_DB_PREFIX[jumpstart]="mnziyan4wc_";
WP_DB_PREFIX[divi]="wcjs3dc2_";


#Optional Variables
#PHP ini pathS
FILE_PHP="C:/wamp3/bin/php/php7.0.29/php.ini";

#Apach config
FILE_APACHE="C:/wamp3/bin/apache/apache2.4.33/conf/httpd.conf";

#Text Editor EXE
TEXT_EDITOR="C:/Program Files/Microsoft VS Code/Code.exe";

GREEN="\033[0;32m";
RED="\033[0;31m";
BLUE="\033[0;34m";
NC="\033[0m"; # No Color

BOLD="\e[1m";

WPAB_CHECKED=false;

#configurable variables
PROJECT_NAME="";

BUILD_TYPE="jumpstart";

DB_NAME="";
DB_FILE_NAME="";

DB_SR_SEARCH="";
DB_SR_REPLACE="";


##############
# Functions
##############

####################################
# WP Build Automation
####################################
handle_arguments () {

	ARGUMENTS=($@);

	for i in "${ARGUMENTS[@]}"
		do

			INDEX=`expr index "$i" =`;

			case $i in
				--project*)
					PROJECT_NAME=${i:INDEX};
				;;
				--build*)
					BUILD_TYPE=${i:INDEX};
				;;
				--dbname*)
					DB_NAME=${i:INDEX};
				;;
				--mysqlun*)
					MYSQL_UN=${i:INDEX};
				;;
				--mysqlpw*)
					MYSQL_PW=${i:INDEX};
				;;
				--dbfilename*)
					DB_FILE_NAME=${i:INDEX};
				;;
				--search*)
					DB_SR_SEARCH=${i:INDEX};
				;;
				--replace*)
					DB_SR_REPLACE=${i:INDEX};
				;;
				*)
					echo -e "${RED}$i is not recognized as an argument.${NC}\n";
				;;
			esac
	done;

	if [[ -z $DB_NAME ]];
		then
			DB_NAME=$PROJECT_NAME;
	fi

}




wp_auto_build () {

	# The main script that runs all the necessary functions to set up a site
	#
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain, and db name
	#
	#  --build 		- [optional]
	# 				- [values] - jumpstart, divi
	# 				- [default] - jumpstart
	# 				- determines whether to install wp-jumpstart or divilib
	#
	#  --dbname		- [optional]
	# 				- [default] - --project
	# 				- determines the name of the database

	TIMESTAMP_START=$( date +%s );
	
	printf "\n\n${BLUE}${BOLD}****************************************\n";
	printf "WP Autobuild Sarting Up...\n";
	printf "****************************************${NC}\n\n";
	

	#check argument number
	if [ $# -eq 0 ]
		then
			printf "${RED}[FAILED] 	No arguments passed. --project is required.\n";
			return;
	fi

	handle_arguments $@;

	echo ;
	echo "***********************************";
	echo "Checking Current Configuration...";
	echo "***********************************";
	echo ;

	# Check variables
	check_config;
	#checks the return code of check_config to make sure everything is good. If not stop script
	if [ $? -eq 1 ]
		then
			return;
		else
			printf "\n${GREEN}Variables are good${NC}\n\n"
	fi

	# Check configure to make sure all commands and dependencies are there
	check_CLI_dependencies;
	#checks the return code of check_CLI_dependencies to make sure everything is good. If not stop script
	if [ $? -eq 1 ]
		then
			return;
		else
			printf "\n${GREEN}Dependencies are good${NC}\n\n"
	fi

	WPAB_CHECKED=true;

	echo "Please check the configuration. Type "Y" and then hit [ENTER] to continue; anything else to stop...";
	read CONTINUE;

	if [[ $CONTINUE != "Y" ]]; then
		return
	fi


	printf "\n***********************************\n";
	printf "Starting: WP Auto-Build...\n";
	printf "***********************************\n";

	#Setup File Structure
	setup_directory;

	#Setup build files
	setup_build;

	#Setup new virtual host
	setup_vhost;

	#Restart Apache
	apache_restart;

	#Setup Build DB
	setup_build_DB;

	#Search and Replace Build DB
	run_SRDB;

	#Update Plugins and Core
	run_wp_update;

	#Renames the theme
	run_rename_theme

	# Dump DB
	db_dump;
	
	# Open in browser
	open_wpab_site;

	TIMESTAMP_END=$( date +%s );

	RUNTIME=$(($TIMESTAMP_END - $TIMESTAMP_START));

	printf "${GREEN}\n";
	printf "***********************************\n";
	printf "Done: WP Auto-Build...\n";
	printf "Time: $RUNTIME secs\n";
	printf "***********************************\n";
	printf "${NC}\n";

}

setup_directory() {
	#
	# Adds a directory for the project and adds an alias in bash goto_{--project}.
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain, and db name
	#

	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi

	printf "${GREEN}Setting up new directory for site...\n";
	printf "************************************${NC}\n";

	# Create new folder for project
	cd "$PATH_DEV";
	mkdir $PROJECT_NAME;

	# Prepend alias to bash
	echo "alias goto_$PROJECT_NAME=\"cd $PATH_DEV/$PROJECT_NAME\"" >> "$FILE_BASH";

	# Open Bash file to make sure alias is added correctly
	open_bash
}


setup_build() {
	# Clones the build repository, deletes the git and then reinitializes it.
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain, and db name
	#
	#  --build 		- [optional]
	# 				- [values] - jumpstart, divi
	# 				- [default] - jumpstart
	# 				- determines whether to install wp-jumpstart or divilib


	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi

	printf "\n${GREEN}Cloning $BUILD_TYPE repository...\n";
	printf "*****************************************${NC}";

	#Check if directory exists
	if [ ! -d "$PATH_DEV/$PROJECT_NAME" ]
		then
			mkdir "$PATH_DEV/$PROJECT_NAME";
	fi

	cd "$PATH_DEV/$PROJECT_NAME";

	echo "";

	#Clone Repository
	git clone ${REPO_PATH[$BUILD_TYPE]} "$PATH_DEV/$PROJECT_NAME";

	#Remove repository .git so we can reinitialize it
	rm -rf "$PATH_DEV/$PROJECT_NAME/.git";

	#Reinitialize Git Respository
	git init "$PATH_DEV/$PROJECT_NAME";

}


setup_vhost () {
	#
	# Adds site to Virtual host and host file
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain
	#

	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi

	printf "\n${GREEN}Creating new virtual host for site...\n";
	printf "*****************************************${NC}\n";


	# Add Virstual Host for Apache
		echo "
# VirtualHost Created (`date`) by WP Auto Build
<VirtualHost *:80>
	ServerName $PROJECT_NAME.local
	DocumentRoot $PATH_DEV/$PROJECT_NAME
	<Directory  \"$PATH_DEV/$PROJECT_NAME\">
		Options +Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride All
		Require local
	</Directory>
</VirtualHost>" >> $FILE_VIRTUALHOST;



	# Add hosts entry to system host
		echo "

# VirtualHost Created (`date`)
127.0.0.1 $PROJECT_NAME.local" >> $FILE_HOST;

	mkdir "$PATH_DEV/$PROJECT_NAME/logs";

	# Open vhosts to make sure everything is configured correctly
		open_vhost

};




apache_restart () {
	printf "\n${GREEN}Restarting Apache...\n";
	printf "*****************************************${NC}\n";

	#Restart apache
	httpd -n wampapache64 -k restart

	#check to make sure APACHE and MYSQL restarted correctly
	check_apache_alive;
	if ! [ $? -eq 0 ]
		then
		    printf "${RED}[FAILED] 	Looks like Apache did not restart correctly${NC}\n\n";
			return;
	fi

	check_mysql_alive;
	if ! [ $? -eq 0 ]
		then
		    printf "${RED}[FAILED] 	Looks like MYSQL did not restart correctly${NC}\n\n";
			return;
	fi
}






setup_build_DB () {
	#
	# Sets up WP wp-config.php file and WP database based on the file in .dev/sql
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain
	#
	# --dbname 	 	- [required]
	# 				- name of the databsae;
	#
	# --build 		- [required]
	# 				- [values] - jumpstart, divi
	# 				- [default] - jumpstart
	# 				- database table prefix
	#
	# --mysqlun	 	- [optional]
	# 				- override default MYSQL_UN;
	#
	# --mysqlpw	 	- [optional]
	# 				- override default MYSQL_PW;
	#

	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi

	printf "\n${GREEN}Generating wp-config.php...\n";
	printf "*****************************************${NC}\n";

	cd "$PATH_DEV/$PROJECT_NAME";

	#Update wp-config.php file with databasee information
	wp config create --dbname="$DB_NAME" --dbuser=$MYSQL_UN --dbpass=$MYSQL_PW --dbprefix="${WP_DB_PREFIX[$BUILD_TYPE]}" --force --extra-php <<PHP
	/** Enable W3 Total Cache */
	define('WP_CACHE', true); // Added by W3 Total Cache


	/** Enable W3 Total Cache Edge Mode */
	define('W3TC_EDGE_MODE', true); // Added by W3 Total Cache


	define('WP_MEMORY_LIMIT', '128M');


	/*
		Limit Post revisions
		- Default: 5
		- No revisions: 0
		- Unlimited Post Revision: -1
	*/
	define( 'WP_POST_REVISIONS', 5 );

	/**
	 * For developers: WordPress debugging mode.
	 *
	 * Change this to true to enable the display of notices during development.
	 * It is strongly recommended that plugin and theme developers use WP_DEBUG
	 * in their development environments.
	 *
	 * For information on other constants that can be used for debugging,
	 * visit the Codex.
	 *
	 * @link https://codex.wordpress.org/Debugging_in_WordPress
	 */
	define('WP_DEBUG', true);
PHP

	printf "\n${GREEN}Creating and importing DB...\n";
	printf "*****************************************${NC}\n";

	#Connect to MYSQL => upload database
	#wp db drop --yes;

	wp db create $DB_NAME;

	if [ $BUILD_TYPE == 'jumpstart' ]; then
		echo "Importing database from $PATH_DEV/$PROJECT_NAME/.dev/wpjs-c8631c0.sql...";
		wp db import "$PATH_DEV/$PROJECT_NAME/.dev/wpjs-c8631c0.sql";
	elif [ $BUILD_TYPE == 'divi' ]; then
		echo "Importing database from $PATH_DEV/$PROJECT_NAME/.dev/divilib.sql...";
		wp db import "$PATH_DEV/$PROJECT_NAME/.dev/divilib.sql";
	fi

}

run_SRDB() {
	#
	# Ruin SRDB on the database
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain
	#
	# --dbname 	 	- [required]
	# 				- name of the databsae;
	#
	# --build 		- [required]
	# 				- [values] - jumpstart, divi
	# 				- [default] - jumpstart
	# 				- database table prefix
	#
	# --mysqlun	 	- [optional]
	# 				- override default MYSQL_UN;
	#
	# --mysqlpw	 	- [optional]
	# 				- override default MYSQL_PW;

	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi

	printf "\n${GREEN}Running SRDB...\n";
	printf "*****************************************${NC}\n";


	#cd "$PATH_DEV/$PROJECT_NAME/srdb";

	if [ $BUILD_TYPE == 'jumpstart' ]; then
		wp search-replace 'wpjs.local' "$PROJECT_NAME.local"
	elif [ $BUILD_TYPE == 'divi' ]; then
		wp search-replace 'divilib.dev' "$PROJECT_NAME.local"
	fi
}

run_wp_update() {
	#
	# Update Wordpress and Plugins
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain
	#
	# --build 		- [required]
	# 				- [values] - jumpstart, divi
	# 				- [default] - jumpstart
	# 				- database table prefix


	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi

	printf "\n${GREEN}Updating WP Core and plugins...\n";
	printf "*****************************************${NC}\n";

	cd "$PATH_DEV/$PROJECT_NAME";

	if [ $BUILD_TYPE == 'jumpstart' ]; then
		#Set License for ACF
		wp option update acf_pro_license "b3JkZXJfaWQ9NzI5MzB8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTAxLTE4IDE4OjMyOjEz"
	elif [ $BUILD_TYPE == 'divi' ]; then
		echo "Add Divi Custom Options"
	fi

	#Update Core
	wp core update;

	#Update All Plugins
	wp plugin update --all;
}

run_rename_theme () {
	#
	# This function renames the theme directory for the new build and then activate it.
	# Removes the old wp-jumpstart theme directory
	#
	# Arguments
	# --project 	- [required]
	# 				- name of the project; deteremines directory name, domain
	#
	# --build 		- [required]
	# 				- [values] - jumpstart, divi
	# 				- [default] - jumpstart
	# 				- database table prefix


	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi


	cd "$PATH_DEV/$PROJECT_NAME/wp-content/themes";

	if [[ $BUILD_TYPE == 'jumpstart' ]]; then

		printf "\n${GREEN}Changing theme directory from /"wpjs/" => /"$PROJECT_NAME/"...\n";
		printf "************************************************************************************${NC}\n";

		# Copy and rename WP Jumpstar theme directory
		cp -R wpjs/ $PROJECT_NAME/;

		#Activate new theme
		wp theme activate $PROJECT_NAME;

		#Remove Old WP Jumpstart theme
		rm -rf wpjs/;

	fi
}

check_config() {
	WPAB_STATUS=true;

	echo "Required Variables...";
	echo "***********************************";

	#Project Name
	if [ -z "$PROJECT_NAME" ]
		then
			printf "${RED}[FAILED] 	PROJECT_NAME is empty. This must be passed with --project.\n";
			WPAB_STATUS=false;
		else

			#Site files
			if [ -d "$PATH_DEV/$PROJECT_NAME" ]
				then
					printf "${RED}[FAILED] 	$PATH_DEV/$PROJECT_NAME is already a project directory.${NC}\n";
					WPAB_STATUS=false;
				else
					printf "${GREEN}[SUCCESS] 	PROJECT_NAME: 		$PROJECT_NAME${NC}\n";
			fi

	fi

	#DB Name
	if [ -z "$DB_NAME" ]
		then
			printf "${RED}[FAILED] 	DB_NAME is empty. ($DB_NAME)\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	DB_NAME: 		$DB_NAME${NC}\n";
	fi

	#Build Type
	if [ $BUILD_TYPE != 'jumpstart' ] && [ $BUILD_TYPE != 'divi' ]
		then
			printf "${RED}[FAILED] 	BUILD_TYPE can olny be /"jumpstart/" or /"divi/" ($BUILD_TYPE).\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	BUILD_TYPE: 		$BUILD_TYPE${NC}\n";
	fi


	#.bashrc
	if [ ! -f "$FILE_BASH" ]
		then
			printf "\n${RED}[FAILED] 	FILE_BASH does not exist ($FILE_BASH). This variable must point to your .bashrc file.${NC}\n";
			WPAB_STATUS=false;
		else
			printf "\n${GREEN}[SUCCESS] 	FILE_BASH: 		$FILE_BASH${NC}\n";
	fi

	#Site files
	if [ ! -d "$PATH_DEV" ]
		then
			printf "${RED}[FAILED] 	PATH_DEV does not exist ($PATH_DEV). This variable is the path to the PARENT of your sites' directory.${NC}\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	PATH_DEV: 		$PATH_DEV${NC}\n";
	fi

	#check repositories
	if [ -z "${REPO_PATH[$BUILD_TYPE]}" ]
		then
			printf "${RED}[FAILED] 	REPO_PATH[$BUILD_TYPE] is empty. This variable should be a reference to the build repository. (${REPO_PATH[$BUILD_TYPE]})${NC}\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	REPO_PATH[$BUILD_TYPE]: 	${REPO_PATH[$BUILD_TYPE]}${NC}\n";
	fi

	#Virtual Host
	if [ ! -f "$FILE_VIRTUALHOST" ]
		then
			printf "${RED}[FAILED] 	FILE_VIRTUALHOST does not exist ($FILE_VIRTUALHOST). This variable must point to Apache's httpd-vhosts.conf file.${NC}\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	FILE_VIRTUALHOST: 	$FILE_VIRTUALHOST${NC}\n";
	fi

	#host
	if [ ! -f "$FILE_HOST" ]
		then
			printf "${RED}[FAILED] 	FILE_HOST does not exist ($FILE_HOST). This variable must point to Window's host file.${NC}\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	FILE_HOST: 		$FILE_HOST${NC}\n";
	fi

	#MYSQL Credentials
	if [ -z "$MYSQL_UN" ]
		then
			printf "${RED}[FAILED] 	MYSQL_UN is empty ($MYSQL_UN). This variable must be set to a MYSQL Username.\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	MYSQL_UN: 		$MYSQL_UN${NC}\n";
	fi

	if [ -z "$MYSQL_PW" ]
		then
			printf "${RED}[FAILED] 	MYSQL_PW is empty ($MYSQL_PW). This variable must be set to a MYSQL Password.\n";
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	MYSQL_PW: 		$MYSQL_PW${NC}\n";
	fi


	echo ;
	echo ;
	echo "Optional Variables...";
	echo "***********************************";

	#check TEXT_EDITOR
	if [ -z "$TEXT_EDITOR" ]
		then
			printf "${RED}[Empty] 	TEXT_EDITOR is empty. This variable must be set to a be able to use any of the /"open_/" commands. Set this to your text editor exe.\n";
		else
			printf "${GREEN}[SUCCESS] 	TEXT_EDITOR: 		$TEXT_EDITOR${NC}\n";
	fi

	#check FILE_PHP
	if [ -z "$FILE_PHP" ]
		then
			printf "${RED}[Empty] 	FILE_PHP is empty. This variable must be set to a be able to use open_php.\n";
		else
			printf "${GREEN}[SUCCESS] 	FILE_PHP: 		$FILE_PHP${NC}\n";
	fi

	#check FILE_APACHE
	if [ -z "$FILE_APACHE" ]
		then
			printf "${RED}[Empty] 	FILE_APACHE is empty. This variable must be set to a be able to use open_apache.\n";
		else
			printf "${GREEN}[SUCCESS] 	FILE_APACHE: 		$FILE_APACHE${NC}\n";
	fi

	#Stop script
	if [ $WPAB_STATUS == false ]
		then
			echo ;
			printf "${RED}Something went wrong. Check the output. Killing process...${NC}\n";
			return 1;
		else
			return 2;
	fi
}

check_CLI_dependencies () {
	WPAB_STATUS=true;

	#check git
	printf "\nChecking git...\n";
	echo "**********************************************";
	if ! [ -x "$(command -v git)" ];
		then
			printf "${RED}[FAILED] 	git is NOT accessible.${NC}\n\n" >&2
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	git is accessible.${NC}\n\n";

			#check access to wp-jumpstart
			printf "Checking: Checking access to ${REPO_PATH[$BUILD_TYPE]}\n";
			git ls-remote "${REPO_PATH[$BUILD_TYPE]}" --exit-code;

			# ls-remote returns anything other than 0, then you cannot access
			if [ "$?" -ne 0 ];
				then
				    printf "${RED}[FAILED] 	Unable to read from ${REPO_PATH[$BUILD_TYPE]}${NC}\n\n";
				    WPAB_STATUS=false;
				else
				    printf "${GREEN}[SUCCESS] 	Access to ${REPO_PATH[$BUILD_TYPE]} is good.${NC}\n\n";
			fi

	fi


	#check mysql
	printf "\nChecking mysql...\n";
	echo "**********************************************";
	if ! [ -x "$(command -v mysql)" ];
		then
			printf "${RED}[FAILED] 	mysql is NOT accessible.${NC}\n" >&2;
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	myqsl is accessible.${NC}\n";

			printf "Checking: Checking credentials...\n";
			check_mysql_alive;

			# check_mysql_access returns anything other than 0, then you cannot access
			if [ "$?" -ne 0 ];
				then
				    printf "${RED}[FAILED] 	Unable to connect to MYSQL. Either credentials are not valid (UN: $MYSQL_UN PW: $MYSQL_PW) or MYSQL is not running. ${NC}\n\n";
				    WPAB_STATUS=false;
				else
				    printf "${GREEN}[SUCCESS] 	Access to MYSQL is good.${NC}\n\n";
			fi
	fi


	#check wp
	printf "\nChecking wp-cli...\n";
	echo "**********************************************";
	if ! [ -x "$(command -v wp)" ];
		then
			printf "${RED}[FAILED] 	wp-cli is not installed correctly.${NC}\n" >&2
			WPAB_STATUS=false;
		else
			printf "${GREEN}[SUCCESS] 	wp-cli is good.${NC}\n"

	fi


	#check xampp start/stop
	printf "\nChecking httpd...\n";
	echo "**********************************************";
	if ! [ -x "$(command -v httpd)" ];
		then
			printf "${RED}httpd is not accessible by the CLI.${NC}\n" >&2
			WPAB_STATUS=false;

		else
			printf "${GREEN}[SUCCESS] 	httpd and Apache restart is good.${NC}\n"

	fi



	#Stop script
	if [ $WPAB_STATUS == false ]
		then
			echo ;
			printf "${RED}Something went wrong. Check the output. Killing process...${NC}\n";
			return 1;
		else
			return 2;
	fi
}

check_mysql_alive() {
	mysql -h localhost -u $MYSQL_UN -p$MYSQL_PW -e"Exit"
}

check_apache_alive() {
	curl --silent --url localhost > /dev\null
}









#Additional commands
#Open files needed to create a virtual host
open_vhost () {
	start "" "$TEXT_EDITOR" "$FILE_VIRTUALHOST";
	start "" "$TEXT_EDITOR" "$FILE_HOST";
};

#open your .bashrc file
open_bash () {
	start "" "$TEXT_EDITOR" "$FILE_BASH";
};

#open this file
open_wpab () {
	start "" "$TEXT_EDITOR" "$FILE_WPAB";
};

#open php.ini file
open_php () {
	start "" "$TEXT_EDITOR" "$FILE_PHP";
}

#open apache's httpd.conf file
open_apache () {
	start "" "$TEXT_EDITOR" "$FILE_APACHE";
}


db_dump () {
	# This function will export the DB
	#
	#
	# Arguments
	# --dbname 	 	- [required]
	# 				- name of the databsae;
	#
	# --dbfilename 	- [optional]
	# 				- set the name of the export file;
	#
	# --project 	- [optional]
	# 				- if specified the function will cd to the project directory and then dump
	# 				- if NOT specified it will try to do it for the current directory
	#
	# --mysqlun	 	- [optional]
	# 				- override default MYSQL_UN;
	#
	# --mysqlpw	 	- [optional]
	# 				- override default MYSQL_PW;

	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi


	# If --project is NOT empty, then cd to that directyory
	if [ ! -z $PROJECT_NAME ]
		then
			cd "$PATH_DEV/$PROJECT_NAME";
	fi

	if [ -z "$DB_FILE_NAME" ]
		then
			DB_FILE_NAME=$DB_NAME;
	fi


	mysqldump -u $MYSQL_UN -p$MYSQL_PW $DB_NAME > ./.dev/$DB_FILE_NAME.sql
	mysqldump -u $MYSQL_UN -p$MYSQL_PW $DB_NAME | gzip > ./.dev/$DB_FILE_NAME.sql.gz

}


db_dump_sr () {
	# This function will export the DB after running a Search & replace on it
	# Returns a gziped and regualr sql file
	#
	#
	# Arguments
	# --project 	- [required]
	# 				- will cd to the project directory and then dump
	#
	# --search 		- [required]
	# 				- Search
	#
	# --replace 	- [required]
	# 				- Replace
	#
	# --dbfilename 	- [optional]
	# 				- set the name of the export file;
	#


	# if false, then we are running the function individually and need to supply arguments, handle arguments and check variables
	if [ $WPAB_CHECKED == 'false' ]
		then
			handle_arguments $@;

			# ########################
			# ADD INDIVIDUAL CHECKS
			# ########################
	fi


	# If --project is a directory then continue
	if [ -d "$PATH_DEV/$PROJECT_NAME" ]
		then
			#Go to project directory
			cd "$PATH_DEV/$PROJECT_NAME";

			# if --dbfilename is empty, filename will be the project name
			if [ -z "$DB_FILE_NAME" ]
				then
					DB_FILE_NAME="$PROJECT_NAME-sr";
			fi

			# Run S&R then export
			# Eport then gzip
			wp search-replace $DB_SR_SEARCH $DB_SR_REPLACE --export=./.dev/$DB_FILE_NAME.sql --all-tables;
			gzip ./.dev/$DB_FILE_NAME.sql;
			# Export plain sql
			wp search-replace $DB_SR_SEARCH $DB_SR_REPLACE --export=./.dev/$DB_FILE_NAME.sql --all-tables;

		else
			echo "Cannot find directory: $PATH_DEV/$PROJECT_NAME"


	fi

}

#open site url in default browser
open_wpab_site () {
	printf "\n${GREEN}Launching Website...\n";
	printf "*****************************************${NC}\n";
	printf "Opening url in default browser...";
	
	start http://$PROJECT_NAME.local
}