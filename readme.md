# WP Auto Build

## Description
WP Autobuild is a Windows shell script that automatically clones and configures a wordpress website. It is designed to save time at the beginning of a Wordpress build by automatically doing much of the setup work.

How it works:

1.  Creates project directory in sites directory and creates shortcut alias in .bashrc file
2.  Clone Code: Wordpress template is cloned from its repository.
3.  Create Hosts: Windows host and Apache virtual host files are appended with new site URL and Apache is restarted.
4.  Create DB and Connect: Creates database and imports data from .dev folder. Creates wp-config.php file and updates db connection.
5.  Search and Replace URL: Search and replace site URL in database.
6.  Update WP: Updates Wordpress core, themes, and plugins.
7.  Rename Theme: Renames theme and activates.
8.  Open Site: Opens built website in default browser.

## Dependencies
* wp-cli



### Setup & Configuration
*  Ensure Windows host and Apache virtual host file has Write privileges by System
*  Include path to script in .bashrc file
```
if [ -f $HOME/wpautobuild/wpautobuild.sh ]; then
	source $HOME/wpautobuild/wpautobuild.sh
fi
```
*  Configure required variables
	* Path to bash file
	* Path to sites root directory
	* Path to WP Autobuild script
	* Path to Apache VirtualHost
	* Path to Windows Host file
	* MySQL Username and Password
	* Project git repository URL
	* Database prefixes


### How to Use
1. Open bash prompt with Administrative privileges.
2. Arguments:

```
--project 		- [required]
				- name of the project; deteremines directory name, domain, and db name

--build 		- [optional]
				- [values] - jumpstart, divi
				- [default] - jumpstart
				- determines whether to install wp-jumpstart or divilib

--dbname		- [optional]
				- [default] - --project
				- determines the name of the database
				
--mysqlun	 	- [optional]
				- override default MYSQL_UN;

--mysqlpw	 	- [optional]
				- override default MYSQL_PW;
					
--search 		- [optional]
				- search string

--replace 		- [optional]
				- replace string

--dbfilename	- [optional]
				- set the name of the db export file;
```
	
3. Example commands
		
Minimum:
	
```
$ wpautobuild --project=[name]
```
		
Define build type and MySQL options:
	
```
$ wpautobuild --project=[name] --build=jumpstart --dbname=override_db_name --mysqlun=override_db_user --override_db_password --search='string to search' --replace='string to replace' --dbfilename=db_export_filename
```